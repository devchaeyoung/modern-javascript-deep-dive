## let

- 재선언 불가
- 업데이트 가능

## const

- 재선언 불가
- 업데이트 불가

```javascript
const greeting = "Hello!";
greeting = "Hi, there!"; // error: Assignment to constant variable.
```

- 객체의 속성 업데이트 가능

```javascript
// 객체로 선언
const greeting = {
  message: "Hello!",
  times: 4,
};

greeting = {
  message: `Hello!, Everyone`,
  name: "elice",
}; // error:  Assignment to constant variable.
```

위의 방식으로는 에러가 발생하지만 아래 방식으로는 변경 가능

```javascript
greeting.message = "Happy Christmas!";
```
